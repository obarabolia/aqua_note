.
=

A Symfony project created on March 23, 2018, 10:16 am.

```
./bin/console doctrine:database:create
./bin/console doctrine:schema:update --dump-sql
./bin/console doctrine:schema:update --force
```
```
./bin/console doctrine:database:drop --force
```

```
./bin/console doctrine:migrations:diff
./bin/console doctrine:migrations:migrate
```

```
./bin/console doctrine:fixtures:load
```