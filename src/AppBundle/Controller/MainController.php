<?php
/**
 * Created by PhpStorm.
 * User: obarabolia
 * Date: 29.03.18
 * Time: 16:56
 */

namespace AppBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homepageAction()
    {
        return $this->render('main/homepage.html.twig');
    }

}