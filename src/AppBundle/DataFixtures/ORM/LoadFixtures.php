<?php
/**
 * Created by PhpStorm.
 * User: obarabolia
 * Date: 30.03.18
 * Time: 15:42
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Loader\NativeLoader;

class LoadFixtures implements ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $loader = new NativeLoader();
        $objectsSet = $loader->loadFile(__DIR__.'/fixtures.yml', [
            'providers' => [$this]
        ]);

        foreach ($objectsSet->getObjects() as $object) {
            $manager->persist($object);
            $manager->flush();
        }
    }
}